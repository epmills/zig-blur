const std = @import("std");
const ArrayList = std.ArrayList;
const iter = @import("iter.zig");
const Cycle = iter.Cycle;

pub fn Take(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const ValueType = T.ValueType;
        inner: T,
        limit: usize,
        iteration: usize,
        orig_inner: T,
        pub fn init(inner: T, limit: usize) Self {
            return Self{
                .inner = inner.clone(),
                .limit = limit,
                .iteration = 0,
                .orig_inner = inner.clone(),
            };
        }
        pub fn clone(self: Self) Self {
            return Self{
                .inner = self.inner.clone(),
                .limit = self.limit,
                .iteration = self.iteration,
                .orig_inner = self.orig_inner.clone(),
            };
        }
        pub fn next(self: *Self) ?ValueType {
            if (self.iteration >= self.limit) {
                return null;
            }
            self.iteration += 1;
            return self.inner.next();
        }
        pub fn take(self: Self, limit: usize) Take(Self) {
            return Take(Self).init(self, limit);
        }
        pub fn cycle(self: Self) Cycle(Self) {
            return Cycle(Self).init(self);
        }
        pub fn collect(self: *Self, list: *ArrayList(ValueType)) !void {
            while (self.next()) |v| {
                try list.append(v);
            }
        }
    };
}
