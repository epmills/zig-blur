const std = @import("std");
const ArrayList = std.ArrayList;
const iter = @import("iter.zig");
const Take = iter.Take;
const Cycle = iter.Cycle;

pub fn UnicodeIterator() type {
    return struct {
        const Self = @This();
        pub const ValueType = u21;
        string: []const u8,
        iteration: usize,
        pub fn init(string: []const u8) Self {
            return Self{ .string = string, .iteration = 0 };
        }
        pub fn clone(self: Self) Self {
            return Self{ .string = self.string, .iteration = self.iteration };
        }
        pub fn next(self: *Self) ?ValueType {
            if (self.iteration >= self.string.len) {
                return null;
            }
            const c = self.string[self.iteration];
            const bsl = std.unicode.utf8ByteSequenceLength(c) catch 0;
            const s = self.string[self.iteration .. self.iteration + bsl];
            const r = byte_sum(s);
            self.iteration += bsl;
            return r;
        }
        pub fn take(self: Self, limit: usize) Take(Self) {
            return Take(Self).init(self, limit);
        }
        pub fn cycle(self: Self) Cycle(Self) {
            return Cycle(Self).init(self);
        }
        pub fn collect(self: *Self, list: *ArrayList(ValueType)) !void {
            while (self.next()) |v| {
                try list.append(v);
            }
        }
        fn byte_sum(bb: []const u8) u21 {
            var sum: u21 = bb[0];
            for (bb[1..]) |b| {
                sum = sum << 8 | b;
            }
            return sum;
        }
    };
}
