const iter = @import("iter.zig");
const Take = iter.Take;

pub fn Cycle(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const ValueType = T.ValueType;
        inner: T,
        orig_inner: T,
        pub fn init(inner: T) Self {
            return Self{
                .inner = inner.clone(),
                .orig_inner = inner.clone(),
            };
        }
        pub fn clone(self: Self) Self {
            return Self{
                .inner = self.inner.clone(),
                .orig_inner = self.orig_inner.clone(),
            };
        }
        pub fn next(self: *Self) ?ValueType {
            var r = self.inner.next();
            if (r == null) {
                self.inner = self.orig_inner.clone();
                r = self.inner.next();
            }
            return r;
        }
        pub fn take(self: Self, limit: usize) Take(Self) {
            return Take(Self).init(self, limit);
        }
    };
}
