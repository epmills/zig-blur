const std = @import("std");
const opts = @import("build_options");
const iter = @import("lib/iter/iter.zig");
const process = std.process;
const ArrayList = std.ArrayList;

const separator = if (builtin.os.tag == .windows) '\r' else '\n';

pub fn main() anyerror!void {
    const stdout = std.io.getStdOut().writer();
    const stderr = std.io.getStdErr().writer();
    const allocator = std.heap.page_allocator;

    // Ensure proper arguments.
    try check_args(allocator, stderr);

    const MessageIterator = iter.UnicodeIterator();
    const KeyIterator = iter.UnicodeIterator();

    var bytes = ArrayList(u8).init(allocator);
    defer bytes.deinit();

    try encipher("Hello world!", "MySecretKey", &bytes);

    try stdout.writeAll(bytes.items);
    try stdout.writeByte('\n');
}

fn encipher(message: []const u8, key: []const u8, bytes: *ArrayList(u8)) !void {
    const bound = 0x10000;

    const MessageIterator = iter.UnicodeIterator();
    var message_iter = &MessageIterator.init(message);

    const KeyIterator = iter.UnicodeIterator();
    var key_iter = &KeyIterator.init(key).cycle();

    var buffer: [4]u8 = undefined;

    while (message_iter.next()) |mcp| {
        var kcp = key_iter.next() orelse unreachable;
        const xcp = @mod(kcp + mcp, bound);
        const len = try std.unicode.utf8Encode(xcp, buffer[0..]);
        try bytes.appendSlice(buffer[0..len]);
    }
}

fn check_args(a: *std.mem.Allocator, out: anytype) !void {
    const EXIT_USAGE = 64;

    var args = process.args();
    _ = args.skip();
    const arg1 = try args.next(a) orelse "";
    if (std.mem.eql(u8, arg1, "-V") or std.mem.eql(u8, arg1, "--version")) {
        try out.print("{}\n", .{opts.build_id});
        process.exit(EXIT_USAGE);
    } else if (arg1.len > 0) {
        try out.print("usage: {} [-h | --help] [-V | --version]\n", .{opts.app_id});
        process.exit(EXIT_USAGE);
    }
}
