# zig-blur

Blurring text through obfuscation


## Usage

**TODO: Implement and demonstrate real usage**

To show a enciphering of the string `Hello world!` with the keyword
`MySecretKey`, simply execute *blurz*:
```shell
$ blurz
Þ¿ÑÒÜã½ÑÝn
```

To show usage and options:
```shell
$ blurz --help
usage: blurz [-h | --help] [-V | --version]
```

To show the version (build ID):
```shell
$ blurz --version
blurz 0.1.0 (326c6d200 2020-08-05)
```


## Installation

The easiest way to install `blurz` is via [Homebrew][]:

```shell
$ brew tap epmills/tap https://gitlab.com/epmills/homebrew-tap.git
```
This only needs to be done once.

Then, to install `blurz`:
```shell
$ brew install blurz
```

NOTE: The Homebrew installation has been used successfully on MacOS.
These instructions should work for Linux and Windows Subsystem for
Linux (WSL) but have not been validated.

If you prefer to install manually, follow the instructions below for
your OS.

### MacOS

Download `blurz_x86_64-macosx-gnu.tar.gz` from [releases][]

Navigate to the folder where the archive was downloaded
```shell
$ cd ~/Downloads
```

Extract the files in the archive:
```shell
$ tar xvzf blurz_x86_64-macosx-gnu.tar.gz
```

If you're using MacOS 10.15 (Catalina) or above, you must take the binary out
of quarantine to execute it:
```shell
$ xattr -r -d com.apple.quarantine blurz/blurz
```

Finally, copy it somewhere in your path:
```shell
$ cp blurz/blurz /usr/local/bin
```

### Linux

Download `blurz_x86_64-linux-musl.tar.gz` from [releases][]

Navigate to the folder where the archive was downloaded
```shell
$ cd ~/Downloads
```

Extract the files in the archive:
```shell
$ tar xvzf blurz_x86_64-linux-musl.tar.gz
```

Finally, copy it somewhere in your path:
```shell
$ cp blurz/blurz /usr/local/bin
```

### Windows

Download `blurz_x86_64-windows-gnu.zip` from [releases][]

Navigate to the folder where the binary was downloaded
```shell
C:\> CD %HOME%\Downloads
```

Extract the files in the archive:
```shell
$ unzip blurz_x86_64-windows-gnu.zip
```

Finally, copy it somewhere in your path:
```shell
C:\Users\Me\Downloads> COPY blurz\blurz.exe %ProgramFiles%
```

[Homebrew]: https://brew.sh/
[releases]: https://gitlab.com/epmills/zig-blur/-/releases