# Makefile for epmills/zig-blur
#  - Must be run from MacOS host

.PHONY: clean

APP_ID = blurz
SRC_FILES = src/main.zig build.zig

BUILD_OPTS = -Drelease-fast=true

TRIPLE_NATIVE = native
TRIPLE_MACOS = x86_64-macosx-gnu
TRIPLE_LINUX = x86_64-linux-musl
TRIPLE_WINDOWS = x86_64-windows-gnu

MAKEFILE_DIR = $(shell pwd)
DIST_DIR = $(MAKEFILE_DIR)/dist
CACHE_DIR = $(MAKEFILE_DIR)/zig-cache
CACHE_BIN_DIR = $(CACHE_DIR)/bin
RUN_BINARY = $(shell find $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_MACOS)*)


clean:
	rm -fR $(CACHE_DIR) $(DIST_DIR)

run: bin.macos
	$(RUN_BINARY)

dist: arc

# Binaries
bin: bin.macos bin.linux bin.windows

bin.macos: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_NATIVE)

bin.linux: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_LINUX)

bin.windows: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_WINDOWS)

# Archives
arc: arc.macos arc.linux arc.win

dist.dir:
	mkdir -p $(DIST_DIR)

arc.linux: dist.dir bin.linux
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_LINUX)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_LINUX)/$(APP_ID); \
	  cd tmp.$(TRIPLE_LINUX); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_LINUX)* $(APP_ID)/$(APP_ID); \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  tar cvzf ../$(APP_ID)_$(TRIPLE_LINUX).tar.gz $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_LINUX);

arc.macos: dist.dir bin.macos
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_MACOS)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_MACOS)/$(APP_ID); \
	  cd tmp.$(TRIPLE_MACOS); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_MACOS)* $(APP_ID)/$(APP_ID); \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  tar cvzf ../$(APP_ID)_$(TRIPLE_MACOS).tar.gz $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_MACOS);

arc.win: dist.dir bin.windows
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_WINDOWS)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_WINDOWS)/$(APP_ID); \
	  cd tmp.$(TRIPLE_WINDOWS); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_WINDOWS)*.exe $(APP_ID)/$(APP_ID).exe; \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  zip ../$(APP_ID)_$(TRIPLE_WINDOWS) $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_WINDOWS);
