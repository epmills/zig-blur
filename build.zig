const builtin = @import("builtin");
const std = @import("std");
const Builder = std.build.Builder;
const Allocator = std.mem.Allocator;

const main = "src/main.zig";
const bin = "blurz";
const version = "0.1.0";

pub fn build(b: *Builder) !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);

    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    const triple = if (target.isNative()) "x86_64-macosx-gnu" else try target.linuxTriple(&arena.allocator);

    // Generate file ID.
    const file_id = b.fmt("{}_{}_{}", .{ bin, version, triple });

    // Collect build date...
    const build_date_args = [_][]const u8{ "date", "+%Y-%m-%d" };
    const build_date = try exec(&arena.allocator, build_date_args[0..]);
    // ...and hash of last git commit...
    const git_hash_args = [_][]const u8{ "git", "rev-parse", "--short=9", "HEAD" };
    const git_hash = try exec(&arena.allocator, git_hash_args[0..]);
    // ...and generate build ID.
    const build_id = b.fmt("{} {} ({} {})", .{ bin, version, git_hash, build_date });

    const exe = b.addExecutable(file_id, main);
    exe.addBuildOption([]const u8, "file_id", dq(b, file_id[0..]));
    exe.addBuildOption([]const u8, "build_id", dq(b, build_id[0..]));
    exe.addBuildOption([]const u8, "app_id", dq(b, bin));
    exe.addBuildOption([]const u8, "triple", dq(b, triple[0..]));
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.single_threaded = true;

    const run_cmd = exe.run();
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    exe.install();

    b.default_step.dependOn(&exe.step);
}

fn exec(a: *Allocator, args: []const []const u8) ![]u8 {
    const result = try std.ChildProcess.exec(.{ .allocator = a, .argv = args[0..] });
    switch (result.term) {
        .Exited => |code| {
            if (code != 0) {
                std.debug.warn("exec error:\n{}\n", .{result.stderr});
                return error.CommandFailed;
            }
        },
        else => {
            std.debug.warn("exec error:\n{}\n", .{result.stderr});
            return error.CommandFailed;
        },
    }
    return result.stdout[0 .. result.stdout.len - 1];
}

fn dq(b: *Builder, s: []const u8) []u8 {
    return b.fmt("\"{}\"", .{s});
}
